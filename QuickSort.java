import java.util.Scanner;


public class QuickSort {
	private static Integer[] array;
	
	public static void main(String [] args){
		Input in=new Input();
		array=new Integer [in.setSize()];
		in.input(array);
		Sort srt=new Sort();
		Output out=new Output();
		out.output(srt.sort(array));
	}
}

class Sort{
	public Integer [] arr;
	
	 public Integer [] sort(Integer [] arr) {
	    if (arr==null||arr.length==0)
		       return null;
	    this.arr=new Integer[arr.length];
	    this.arr=arr;
	    int size=this.arr.length;
	    qSort(0, size-1);
	    return this.arr;
	 }
	
	public void qSort(int start, int end){
		try{
			if (start>=end)
	            return;
	        int i=start,j=end;
	        int cur=i-(i-j)/2;
		    while (i<j){
	            while(i<cur&&(arr[i]<=arr[cur])){
	            	i++;
	            }
	            while(j>cur&&(arr[cur]<=arr[j])) {
	                j--;
	            }
	            if (i<j) {
	                int temp=arr[i];
	                arr[i]=arr[j];
	                arr[j] = temp;
	                if (i==cur)
	                   cur=j;
	                else if (j==cur)
	                   cur=i;
	            }
	        }
	        qSort(start, cur);
	        qSort(cur+1, end);
		
		}
		catch(Exception e){
			e.printStackTrace(); 
			System.exit(1);
		}
	}
}


class Input{	
	int size;
	public int setSize(){
		try{
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter the size of array");
			size=sc.nextInt();
			return size;
		}	
		catch(Exception e){
			System.err.println("Incorrect array size"); 
			System.exit(1);
		}
		return 0;
	}
	
	public Integer[] input(Integer[] arr){
		try{
			System.out.println("Initialize the array");
			Scanner sc=new Scanner(System.in);
			String s=sc.nextLine();
			int i=0;
			for (String prop: s.split(",")){
				arr[i]=Integer.parseInt(prop);
				i++;
		    }
			if(i<arr.length){
				System.err.println("The dimension of the array must be "+size); 
				System.exit(1);
			}		
		}
		catch(Exception e){
			System.err.println("Incorrect data type"); 
			System.exit(1);
		}	
		return arr;
	}
}

class Output{
	public Integer[] output(Integer [] arr){ 
			for(int i=0,l=arr.length;i<l;i++)
				System.out.print(arr[i]+" ");
		return arr;
	}
}

